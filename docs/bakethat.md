# bakethat
Script internal to the container, that runs a configured bitbake based on the configuration controlled environment.

- Call ``bakethat`` exactly as you would ``bitbake``, arguments provided to it will be passed directly to the underlying ``bitbake``.
- It is in ``$PATH`` once inside the container.
- It can be used in ``run-builder``'s interactive mode, with additional behaviors. See below.
- By default, bitbake output is appened to an a log file named after the configured ``$BS_TUPLE``, via ``| tee``

## Usage

```
$ bakethat package-name [-c clean, et al]
```

## Alternate behavior
Alternate behavior can be used when in ``run-builder -i`` mode.

- ``SKIP_INIT`` : Skip the ``repo init`` call during environment setup.
- ``SKIP_SYNC`` : Skip the ``repo sync`` call during environment setup. This will automatically enable ``SKIP_SYNC``
- ``NOLOG`` : Do not ``tee`` to the log file. This enables ``bitbake``'s knotty interactive interface, instead of scrolling logging.

To use any of these, ``export`` them with any value. To disable them, ``export`` them with an empty value (``export NOLOG=``)
