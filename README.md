# Dockbake (bitbake in docker)

## What & Why
[Docker][]-powered, parameterized building with [Bitbake][]. This is done because re-creating these environments every time is a complete pain, and we need to automate it too (Jenkins). Added bonus: large scale eliminiation/identification of host bleed.

## How
We make use of [Docker][] to handle a reproducable [LXC][] containers that isolate the running of bitbake into a uniform environment. This is then extended by the use of host directory mapping and sharing download directories for all recipes on all platforms. 

Per-configuration trees are seperated with via tuple, in this pattern:
``${BB_DISTRO}-${BB_DISTRO_TYPE}-${BB_MACHINE}``

So that you end up with a tree looking like this:

```
|-- build
|   `-- distro-debug-machine
|-- images
|   `-- distro-debug-machine
`-- sstate
    `-- distro-debug-machine
```

## Getting Running
#### This system
Setup the runtime environment

```
git clone git@bitbucket.org:WarheadsSE/dockbake.git
cd dockbake
bin/make-environment
```

#### Docker & Dependancies
You will then need to install [Docker](https://www.docker.io/gettingstarted/#h_installation) from <https://www.docker.io/gettingstarted/#h_installation> per your distributions' needs.

After you have installed docker, to remove the need of calling sudo for all commands, add your user to the ``docker`` group. Create it if it does not exist.


Once that is configured and operational, we can build the ``bitbake`` container that this system will use.

```
sudo docker build -t bitbake docker/bitbake/
```

#### Configuration

* See docs explicit details of the scripts and their operation
* See ``conf/local.in`` for setting local defaults.

Fire off a build of the default(s) with `` bin/run-builder ``. Might require ``sudo`` depending on your configuration.

## Operation
### File Destinations
Running ``make-environment`` will result in the below file structure is ensured to exist:

```
.
|-- bin
|-- build
|-- conf
|-- dl
|-- etc
|-- images
`-- sstate
```

By default, these scripts will use as below:

- ./build as ``OE_BASE`` in the container, with naming tuple appended on the host.
- ./dl as ``DL_DIR`` in the container.
- ./images as ``DEPLOY_IMAGE_DIR`` in the container, with naming tuple appended on the host.
- ./sstate as ``SSTATE_DIR`` in the container, with naming tuple appended on the host.

**NOTE:** It is possible for ``DL_DIR``, ``DEPLOY_DIR`` and ``SSTATE_DIR`` can be mount points to network fileshares.


### Configuration
The contents of the conf directory are simple bash scripts with ``export`` calls for the environment variables passed into the builder instance.

### Commands
- make-environment
- run-builder


[bitbake]: <http://www.yoctoproject.org/docs/current/mega-manual/mega-manual.html>
[docker]: <http://docker.io>
[lxc]: <http://linuxcontainers.org>
